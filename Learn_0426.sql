-- [創建database]
create database `sql_tutorial`;
show databases;
-- [刪除使用Drop]
drop database `sql_tutorial`;
show databases;

USE `sql_tutorial`;

create table student(
	`student_id` int primary key,
    `name` varchar(30),
    `Major` varchar(20)
);

describe `student`; -- describe 顯示table
drop table `student`; -- 刪除table

-- 增加欄位
alter table `student` add gpa decimal(3,2);
describe `student`;
-- 刪除欄位
alter table `student` drop column gpa;
describe `student`;

-- 資料格式
-- INT -- 整數
-- DECIMAL(5,6) -- 有小數點
-- VARCHAR(10) -- 字串
-- BLOB -- (binary large object) 圖片 影片 檔案
-- DATE -- "YYYY-MM-DD" 日期
-- TIMESTAMP -- "YYYY-MM-DD HH:MM:SS" 紀錄時間