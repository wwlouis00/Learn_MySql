create database `test`;
show databases;
use `test`;
create table `student`(
	`id` INT PRIMARY KEY,
	`name` varchar(20),
	`major` varchar(20)
);
describe `student`;
drop table `student`;
alter table`student` add gpa decimal(3,2)